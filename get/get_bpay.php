<?php

require_once('../classes/pay.php');



    $reference = 'REF-'.rand();
    $description  = "Pago basico prueba";
    $amount = array("currency"=> "COP","total"=>"1500");
  //  $returnUrl = 'http://esteban.epizy.com/placetopay/';
    $returnUrl = 'https://dev.placetopay.com/redirection/sandbox/session/'.rand();
    //$ipAddress = "127.0.0.1";
    $ipAddress = "185.27.134.11";
    $userAgent = "Place to pay prueba sandbox";
    $minutes_to_add = 5;
    $time = new DateTime('c');
    $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));
    $expiration = $time->format('Y-m-d H:i');




    $args = array(
        "reference" => $reference,
        "description" => $description,
        "amount" => $amount,
        "expiration" => $expiration,
        "returnUrl" => $returnUrl,
        "ipAddress" => $ipAddress ,
        "userAgent" => $userAgent
    );


    $pay = new pay();
    $response = $pay->get_pay($args);

    echo $response;
    exit();