<?php 
require_once('main.php');
class pay extends main{

	function __construct(){
		parent::__construct();

	}

	public function get_pay($params){
		

		$arr = array(
			"auth"=>$this->params_['auth'],
			"reference" => $params['reference'],
			"description" => $params['description'],
			"amount" => $params['amount'],
			"expiration" => $params['expiration'],
			"returnUrl" => $params['returnUrl'],
			"ipAddress" => $params['ipAddress'],
			"userAgent" => $params['userAgent']
		);

		return json_encode($arr);
	}

	public function get_endpoint_topay(){
		return $this->endpoint;
	}


}