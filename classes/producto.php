<?php
require_once('main.php');
class producto extends main{

	function __construct(){
		parent::__construct();

	}

	function all(){
		$args = array(
			'post_type' => 'product',
			'post_per_page' => -1
		);
		$productos = new WP_Query($args);
		return json_encode($productos->posts);

	}

/*
	function filterCategoryByProductId($category_id){
		global $wpdb;
		$sql = "SELECT t.*, tt.*
		FROM wp_terms AS t 
		INNER JOIN wp_term_taxonomy AS tt ON tt.term_id = t.term_id 
		INNER JOIN wp_term_relationships AS tr ON tr.term_taxonomy_id = tt.term_taxonomy_id
		WHERE tt.taxonomy IN ('product_cat') AND tr.term_taxonomy_id  =".$category_id;
		$category = $wpdb->get_results($sql);

		return  json_encode($category[0]);

	}*/

	function filterByCategoryId($category_id){	
		global $wpdb;
		$sql = "SELECT SQL_CALC_FOUND_ROWS wp_posts.ID,wp_posts.post_title,wp_posts.post_content FROM wp_posts LEFT JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) WHERE 1=1 AND ( wp_term_relationships.term_taxonomy_id IN ('".$category_id."') ) AND wp_posts.post_type = 'product' AND ((wp_posts.post_status = 'publish')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_title ASC ";



		$wp_posts =  $wpdb->get_results($sql);

		$products = array();

		foreach($wp_posts as $i => $post):
			$src_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full',true);

			$img = explode(", ",$src_img[0]);
			$thumbnail = (wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ))==false)?'http://localhost/wordpress/wp-content/themes/wordpress/assets/img/default_image.png': $img[0];

			$_product = wc_get_product( $post->ID );


			$price = $_product->get_regular_price();
			$discount_price = $_product->get_sale_price();
			/*$_product->get_price();*/
			$discount_porcent = (empty(get_post_meta($post->ID,'product_discount',true)) || is_nan(get_post_meta($post->ID,'product_discount',true))) ?  0 : get_post_meta($post->ID,'product_discount',true);

			$children_ids = $_product->get_children();
			$permalink = get_permalink($post->ID);
			$apifacebook = '595489497242932';
			$facebook_share_uri = 'https://www.facebook.com/sharer.php?u='.$permalink.'&amp;quote'.$post->post_title.';v=3';
			$facebook_msg_uri = 'https://www.facebook.com/dialog/send?app_id='.$apifacebook.'&amp;display=popup'.'&amp;link='.$permalink.';redirect_uri='.$permalink;    
			$twitter_share_uri = 'https://twitter.com/intent/tweet?url='.$permalink.';text='.$post->post_title;
			$whatsapp_share_uri = 'https://wa.me/?text='.$permalink;


			$attachment_ids = $_product->get_gallery_attachment_ids();
			$all_images = array();
			
			$product_id = strval($_product->id);
			$product = new WC_product($product_id);
			$attachment_ids = $product->get_gallery_attachment_ids();

			
			foreach ($attachment_ids as $attachment_id) {
				array_push($all_images, wp_get_attachment_url($attachment_id));
			}
			if (has_post_thumbnail($_product->id)) {
				$attachment_ids[0] = get_post_thumbnail_id($_product->id);
				$featured_img = wp_get_attachment_image_src($attachment_ids[0], 'full');
			} 



			array_push(	$products, array(
				'id' => $post->ID,
				'title' => $post->post_title,
				'description' => $post->post_content,
				'thumbnail' => $thumbnail,
				'price' => $price,
				'discount_price' => $discount_price,
				'discount_porcent' => $discount_porcent,
				'permalink' => $permalink,
				'apifacebook' => $apifacebook,
				'facebook_share_uri' => $facebook_share_uri,
				'facebook_msg_uri' => $facebook_msg_uri,
				'twitter_share_uri' => $twitter_share_uri,
				'whatsapp_share_uri' => $whatsapp_share_uri,
				'description' => $post->post_content,
				'all_images' => array($all_images),


			));

		endforeach;
		return json_encode($products);

	}
	/*
		thumb-images
	*/



	/*
	
	*/
	function getProductsByCategId($category_id){
		
		global $wpdb;
		$sql = "SELECT SQL_CALC_FOUND_ROWS wp_posts.ID,wp_posts.post_title,wp_posts.post_content FROM wp_posts LEFT JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) WHERE 1=1 AND ( wp_term_relationships.term_taxonomy_id IN ('".$category_id."') ) AND wp_posts.post_type = 'product' AND ((wp_posts.post_status = 'publish')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ";



		$wp_posts =  $wpdb->get_results($sql);

		$products = array();

		foreach($wp_posts as $i => $post):
			$src_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full',true);

			$img = explode(", ",$src_img[0]);
			$thumbnail = (wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ))==false)?'http://localhost/wordpress/wp-content/themes/wordpress/assets/img/default_image.png': $img[0];

			$_product = wc_get_product( $post->ID );


			$price = $_product->get_regular_price();
			$discount_price = $_product->get_sale_price();
			/*$_product->get_price();*/
			$discount_porcent = empty(get_post_meta($post->ID,'product_discount',true)) ?  0 : get_post_meta($post->ID,'product_discount',true);


			$children_ids = $_product->get_children();
			$permalink = get_permalink($post->ID);
			$apifacebook = '595489497242932';
			$facebook_share_uri = 'https://www.facebook.com/sharer.php?u='.$permalink.'&amp;quote'.$post->post_title.';v=3';
			$facebook_msg_uri = 'https://www.facebook.com/dialog/send?app_id='.$apifacebook.'&amp;display=popup'.'&amp;link='.$permalink.';redirect_uri='.$permalink;    
			$twitter_share_uri = 'https://twitter.com/intent/tweet?url='.$permalink.';text='.$post->post_title;
			$whatsapp_share_uri = 'https://wa.me/?text='.$permalink;


			$attachment_ids = $_product->get_gallery_attachment_ids();
			$all_images = array();
			
			foreach ($attachment_ids as $attachment_id) {
				/* array_push($all_images, wp_get_attachment_url($attachment_id));*/
				array_push($all_images,basename ( get_attached_file( $attachment_id ) ));
			}

			array_push(	$products, array(
				'id' => $post->ID,
				'title' => $post->post_title,
				'description' => $post->post_content,
				'thumbnail' => $thumbnail,
				'price' => $price,
				'discount_price' => $discount_price,
				'discount_porcent' => $discount_porcent,
				'permalink' => $permalink,
				'apifacebook' => $apifacebook,
				'facebook_share_uri' => $facebook_share_uri,
				'facebook_msg_uri' => $facebook_msg_uri,
				'twitter_share_uri' => $twitter_share_uri,
				'whatsapp_share_uri' => $whatsapp_share_uri,
				'description' => $post->post_content,
				'all_images' => array($all_images),



			));


		endforeach;
		return json_encode($products);
	}


	/*

	*/
	function searchByKeyWord($search){

		$args = array(
			'post_type' => 'product',
			's' => $search,
			'post_status' => 'publish'

		);


		$products = new WP_Query($args);

		$all_results = array();

		foreach($products->posts as $i => $product){

			$permalink = get_the_permalink($product->ID);
			$src_img = wp_get_attachment_image_src(get_post_thumbnail_id($product->ID),'full',true);
			$img = explode(",",$src_img[0]);
			$thumbnail = (wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ))==false)?'http://localhost/wordpress/wp-content/themes/wordpress/assets/img/default_image.png': $img[0];
			$image = explode(",",$thumbnail);
			$name = $product->post_title;
			$regularprice = get_post_meta($product->ID, '_regular_price', true);
			$_product = wc_get_product( $product->ID );
			$discount_price = $_product->get_sale_price();
			array_push($all_results, array(
				'id' => $product->ID,
				'permalink' => $permalink,
				'image' => $image[0],
				'name' => $name,
				'regular_price' => $regularprice,
				'discount_price' => $discount_price,




			));


		}



		echo json_encode($all_results);
		exit();




	}


}
