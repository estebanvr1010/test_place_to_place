<?php
require_once('DBSettings.php');
class main extends DatabaseSettings {
	public $params_ = array();
	public $endpoint = "https://test.placetopay.com/redirection/";


	var $classQuery;
	var $link;
	
	var $errno = '';
	var $error = '';

	function __construct(){
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Credentials: true");
 		header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		 //header('Content-Type: application/json');
		
		$secretKey = rand();
		$nonce = rand();
		$seed = date('c');
		$tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));
		$this->params_['auth'] = array(
				 		"login" 	=> "6dd490faf9cb87a9862245da41170ff2",
				 		"seed"		=> $seed,
				 		/*"nonce" 	=> "ZjgzYzNkNzI1MmEwNjRlNzlhZDViOGIyNmQxNjcxZTg=",*/
				 		"nonce" 	=> $nonce,
				 		/*"tranKey"	=> "gUwHkL4tiU+0oCWEiWw0q2uUfwQ="*/
				 		"tranKey"	=> $tranKey
		 			);
	 
	}




// database
	function DBClass()
	{
		$settings = DatabaseSettings::getSettings();
		
		$host = $settings['dbhost'];
		$name = $settings['dbname'];
		$user = $settings['dbusername'];
		$pass = $settings['dbpassword'];
		
		$this->link = new mysqli( $host , $user , $pass , $name );
	}
	
	function query( $query ) 
	{
		$this->classQuery = $query;
		return $this->link->query( $query );
	}
	
	function escapeString( $query )
	{
		return $this->link->escape_string( $query );
	}
	
	function numRows( $result )
	{
		return $result->num_rows;
	}
	
	function lastInsertedID()
	{
		return $this->link->insert_id;
	}
	
	function fetchAssoc( $result )
	{
		return $result->fetch_assoc();
	}
	
	function fetchArray( $result , $resultType = MYSQLI_ASSOC )
	{
		return $result->fetch_array( $resultType );
	}
	
	function fetchAll( $result , $resultType = MYSQLI_ASSOC )
	{
		return $result->fetch_all( $resultType );
	}
	
	function fetchRow( $result )
	{
		return $result->fetch_row();
	}
	
	function freeResult( $result )
	{
		$this->link->free_result( $result );
	}
	
	function close() 
	{
		$this->link->close();
	}
	
	function sql_error()
	{
		if( empty( $error ) )
		{
			$errno = $this->link->errno;
			$error = $this->link->error;
		}
		return $errno . ' : ' . $error;
	}






}