<?php
class DatabaseSettings
{
	
	var $settings;
	public $dbhost = '127.0.0.1';
	public $dbname = 'placetopaydb';
	public $dbusername = 'placetopayuser';
	public $dbpwd = 'placetopay123';

	function getSettings()
	{
		// Database variables
		// Host name
		$settings['dbhost'] = $this->dbhost;
		// Database name
		$settings['dbname'] = $this->dbname;
		// Username
		$settings['dbusername'] = $this->dbusername;
		// Password
		$settings['dbpassword'] = $this->dbpwd;
		
		return $settings;
	}
}